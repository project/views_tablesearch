let charstart = 2;
let hColor = '#ccc';
let hElement = 'span';
let hClass = 'ts-highlight';
let caseSensitiveSearch = 0;

(function ($, Drupal, drupalSettings) {

  if (drupalSettings.tablesearch.characters) {
    charstart = drupalSettings.tablesearch.characters;
  }
  if (drupalSettings.tablesearch.highlightColor) {
    hColor = drupalSettings.tablesearch.highlightColor;
  }
  if (drupalSettings.tablesearch.highlightElement) {
    hElement = drupalSettings.tablesearch.highlightElement;
  }
  if (drupalSettings.tablesearch.highlightClass) {
    hClass = drupalSettings.tablesearch.highlightClass;
  }
  if (drupalSettings.tablesearch.caseSensitiveSearch) {
    caseSensitiveSearch = drupalSettings.tablesearch.caseSensitiveSearch;
  }
})(jQuery, Drupal, drupalSettings);

(function ($) {
  function checkTable(tableNameTr, tableNameTd, searchString, length, highlightEl, highlightClass, highlightColor, caseSensitiveSearch) {
    let found = 0;
    let x2 = '';
    let m = '';

    $(tableNameTr).each(function () {
      found = 0;

      $(this).find(tableNameTd).each(function () {
        const text = $(this).text().trim();
        const textNotCaseSensitive = text.toLowerCase();
        const searchStringNotCaseSensitive = searchString.toLowerCase();
        $(this).find(`${highlightEl}.${highlightClass}`).contents().unwrap();
        let html = $(this).html();
        let x = 0;

        if (caseSensitiveSearch) {
          if (text.indexOf(searchString, x) >= 0) {
            x = text.indexOf(searchString);
            x2 = x + length;
            m = ` <${highlightEl} class = "${highlightClass}" style = "background-color:${highlightColor};">${text.substr(x, x2 - x)}</${highlightEl}>`;
            m = m.trim();
            const regex = new RegExp(text.substr(x, x2 - x), 'g');
            if (html.indexOf('<a href') === -1 || html.indexOf('</p><p><a href') !== -1) {
              html = html.replace(regex, m);
              $(this).html(html.replace(text, m));
            } else {
              const newText = text.replace(regex, m);
              $(this).html(html.replace(text, newText));
            }
            found = 1;
          }
        } else {
          if (textNotCaseSensitive.indexOf(searchStringNotCaseSensitive, x) >= 0) {
            x = textNotCaseSensitive.indexOf(searchStringNotCaseSensitive);
            x2 = x + length;
            m = ` <${highlightEl} class = "${highlightClass}" style = "background-color:${highlightColor};">${text.substr(x, x2 - x)}</${highlightEl}>`;
            m = m.trim();
            const regex = new RegExp(text.substr(x, x2 - x), 'g');
            if (html.indexOf('<a href') === -1 || html.indexOf('</p><p><a href') !== -1) {
              html = html.replace(regex, m);
              $(this).html(html.replace(text, m));
            } else {
              const newText = text.replace(regex, m);
              $(this).html(html.replace(text, newText));
            }
            found = 1;
          }
        }
      });

      if (found === 0) {
        $(this).closest('tr').css('display', 'none');
      } else {
        $(this).closest('tr').css('display', 'table-row');
      }
    });
  }

  function clearTable(tableNameTr, tableNameTd, highlightEl, highlightClass) {
    $(tableNameTr).each(function () {
      $(this).find(tableNameTd).each(function () {
        $(this).find(`.${highlightClass}`).contents().unwrap();
      });
    });
    $(tableNameTr).css('display', 'table-row');
  }

  function tableSearch(options) {
    $(options.input).keyup(function () {
      const value = $(this).val();
      const length = value.length;

      if (length >= options.start) {
        checkTable(options.tr, options.td, value, length, options.highlightEl, options.highlightClass, options.highlightColor, caseSensitiveSearch);
      } else {
        clearTable(options.tr, options.td, options.highlightEl, options.highlightClass);
      }
    });
  }

  $(document).ready(function () {
    const options = {
      input: '#searchableinput',
      start: charstart,
      tr: 'table.table-ts tbody tr',
      td: '.views-tablesearchable-1',
      highlightEl: hElement,
      highlightClass: hClass,
      highlightColor: hColor,
    };
    tableSearch(options);
  });

  $('#casesensitive').change(function () {
    caseSensitiveSearch = $(this).is(':checked') ? 1 : 0;
  });
})(jQuery);
