## INTRODUCTION

* **Views Table Search** can be used to create views with JavaScript-enabled 
search for table records. It provides responsive search functionality, 
highlights the results where the search criteria are met, and hides rows that 
doesn't contain the search keyword. It is highly customizable: you can choose
Views Table Search settings for each view you create.

* The JavaScript used is a plugin created for this project and can be used 
independently inside a Drupal project or in any other application or website.

## REQUIREMENTS

* Views Table Search requires Drupal and the core Views module enabled.

## INSTALLATION

* Install as you would normally install a contributed Drupal module. 
See the [Drupal Instructions On Installing Modules]
(https://www.drupal.org/docs/extending-drupal/installing-modules) 
in the Drupal documentation for further information.

## CONFIGURATION

* Configuration is on a per view/display basis. Select 'Views Table Search' 
as the display format and then configure settings as desired under 
Format Settings.

* The Views Table Search template includes a "table" display for the result. 
However, a template can be created for each view (or copied from the module 
templates folder to the theme), and table elements can be completely removed. 
In this case, the JavaScript file (views_tablesearch.js) might need to be 
updated to function without table/tr/td elements. This feature might be added
in future versions, along with an option to display search results statistics
(e.g., the number of results found).
